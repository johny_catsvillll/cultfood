<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Admin::routes();
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
});

Route::get('/cache-clear', function (){
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    return redirect()->back();
});

Route::get('/parents', [
    'uses' => 'AuthController@auth'
]);

Route::get('/save-shortcode', [
    'uses' => 'GlobalController@saveShortcodes'
]);

Route::get('/remove-shortcode', [
    'uses' => 'GlobalController@removeShortcodes'
]);

Route::get('/export-excel', [
    'uses' => 'GlobalController@export',
    'as' => 'export-excel'
]);

Route::get('/blog', [
    'as' => 'blog',
    'uses' => 'NewsController@blog'
]);

Route::get('/blog/{id}-{slug}', [
    'as' => 'one-blog',
    'uses' => 'NewsController@oneNews'
]);


Route::get('/news/{id}-{slug}', [
    'as' => 'one-news',
    'uses' => 'NewsController@oneNews'
]);



Route::get('/oauth/{driver}', 'AuthController@redirectToProvider');
Route::get('/oauth/{driver}/callback', 'AuthController@handleProviderCallback');

$pages = \App\Page::get()->pluck('slug')->toArray();




Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@home'
]);

Route::get('/faq', [
    'as' => 'faq',
    'uses' => 'FaqController@faq'
]);

Route::get('/news', [
    'as' => 'news',
    'uses' => 'NewsController@news'
]);

Route::get('/liders', [
    'as' => 'liders',
    'uses' => 'LiderController@liders'
]);

Route::get('/show-liders', [
    'uses' => 'LiderController@showLiders'
]);



Route::get('/show-articles', [
    'uses' => 'NewsController@showArticles'
]);

Route::get('/setting', function () {
    return view('setting');
});

Route::get('/save-cookie', [
    'uses' => 'GlobalController@saveCookie'
]);

Route::get('/chat', function () {
    return view('chat');
});

Route::get('/account-liders', function () {
    return view('account-liders');
});

Route::get('/auth', [
    'as' => 'auth',
    'uses' => 'AuthController@auth'
]);

Route::post('/registration', [
    'as' => 'registration',
    'uses' => 'AuthController@registration'
]);

Route::post('/login', [
    'as' => 'login',
    'uses' => 'AuthController@login'
]);


Route::get('/counter-download-pdf', [
    'uses' => 'GlobalController@counterDownloadPdf'
]);

Route::get('/otg', [
    'uses' => 'OtgController@otg'
]);

Route::get('/show-otg', [
    'uses' => 'OtgController@showOtg'
]);

Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'AuthController@logout'
]);

Route::group(['as' => 'account.', 'prefix' => 'account', 'middleware' => ['checkUser']], function() {

    Route::match(['get', 'post'], '/', [
        'as' => 'home',
        'uses' => 'AccountController@home'
    ]);

    Route::get('/instruction', [
        'as' => 'instruction',
        'uses' => 'AccountController@instruction'
    ]);

    Route::get('/liders', [
        'as' => 'liders',
        'uses' => 'AccountController@liders'
    ]);

    Route::get('/chat', [
        'as' => 'chat',
        'uses' => 'AccountController@chat'
    ]);

    Route::post('/save-message', [
        'as' => 'save-message',
        'uses' => 'AccountController@saveMessage'
    ]);

    Route::get('/save-current-area', [
        'uses' => 'AccountController@saveCurrentArea'
    ]);

    Route::get('/save-step-account', [
        'uses' => 'AccountController@saveStep'
    ]);

});

Route::get('/{page}', [
    'uses' => 'GlobalController@page'
])->where('slug', implode('|', $pages));



