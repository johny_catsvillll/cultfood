<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Region extends Model
{
    public function liders() {
        return $this->hasMany(Lider::class, 'region_id');
    }
}
