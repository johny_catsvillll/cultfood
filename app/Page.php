<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAdminPanel\Traits\Cropper;


class Page extends Model
{
    use Cropper;
}
