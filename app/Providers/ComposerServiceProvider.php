<?php

namespace App\Providers;

use App\MainMenu;
use App\News;
use App\Page;
use Illuminate\Support\ServiceProvider;
use Request;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('seo', $this->getSeoPage());
        view()->share('articles', $this->getArticles());
        view()->share('mainMenu', $this->getMenu());
    }

    public function getMenu() {
        return MainMenu::orderBy('order')->get();
    }

    public function getSeoPage() {

        $data = false;

        $path = Request::path();

        if(!$path) {
            $path = '/';
        }

        $page = Page::where('slug', $path)->first();

        if($page) {
            return $page;
        }


        return $data;

    }

    public function getArticles() {
        return News::orderBy('created_at', 'desc')->take(4)->get();
    }
}
