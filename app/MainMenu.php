<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainMenu extends Model
{
    public function pageId() {
        return $this->belongsTo(Page::class, 'page_id');
    }
}
