<?php
namespace App\Exports;
use App\Account as AccountModel;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Account implements FromCollection, WithHeadings
{
    use Exportable;

    public function __construct($dateFrom, $dateTo){
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function collection()
    {
        return AccountModel::select('id', 'created_at', 'name', 'last_name', 'email')->where('created_at', '>', $this->dateFrom)->where('created_at', '<=', $this->dateTo)->get();
    }

    public function headings() : array
    {
        return $headings = [
            'Ідентифікатор',
            'Дата створення',
            "Ім'я",
            'Прізвище',
            'Пошта',
        ];
    }


}