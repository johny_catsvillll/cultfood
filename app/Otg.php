<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Otg extends Model
{
    public function regionId() {
        return $this->belongsTo(Region::class, 'region_id');
    }
}
