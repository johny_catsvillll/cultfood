<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function oneNews(Request $request, $id, $slug) {

        $news = News::where('id', $id)->where('slug', $slug)->where('active', 1)
            ->firstOrFail();

        $news->post_content = renderShortCodes($news->post_content);


        $prevNews = News::where('id', '<', $id)->orderBy('id', 'desc')
            ->where('active', 1)
            ->first();
        if(!$prevNews) {
            $prevNews = News::orderBy('id', 'desc')
                ->where('active', 1)
                ->first();
        }

        $nextNews = News::where('id', '>', $id)
            ->where('active', 1)
            ->first();


        if(!$nextNews) {
            $nextNews = News::where('active', 1)
                ->first();
        }


        return view('one-news', compact('news', 'prevNews', 'nextNews'));
    }

    public function showArticles(Request $request) {


        $news = News::orderBy('created_at', 'desc')->where('active', 1);

        if($request->type == 'news') {
            $news->where('category_id', 1);
        }
        elseif($request->type == 'blog') {
            $news->where('category_id', 2);
        }


        $news = $news->paginate(4);
        if($request->ajax()) {

            $lastPage = $news->lastPage();
            if($request->type == 'news') {
                $view = view('blocks.list-news', compact('news'))->render();
            }
            else {
                $view = view('blocks.list-articles', compact('news'))->render();
            }



            return ['html' => $view, 'lastPage' => $lastPage];

        }

    }

    public function news() {


        $news = News::orderBy('date_publication', 'desc')
            ->where('category_id', 1)
            ->where('active', 1)
            ->where('date_publication', '<', now())
            ->paginate(4);

        $lastPage = $news->lastPage();


        return view('news', compact('news', 'articles', 'lastPage'));
    }

    public function blog() {


        $news = News::orderBy('date_publication', 'desc')
            ->where('category_id', 2)
            ->where('active', 1)
            ->where('date_publication', '<', now())
            ->paginate(4);

        $lastPage = $news->lastPage();

        return view('news', compact('news', 'articles', 'lastPage'));
    }
}
