<?php

namespace App\Http\Controllers;

use App\Lider;
use App\Region;
use Illuminate\Http\Request;

class LiderController extends Controller
{
    public function liders() {

        $regions = Region::has('liders', '>=', 1)->get();

        return view('liders', compact('regions'));
    }

    public function showLiders(Request $request) {


        $liders = Lider::where('region_id', $request->id)->where('active', 1)->get();
        $view = view('blocks.list-liders', compact('liders'))->render();

        return ['html' => $view];
    }
}
