<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function auth() {
        return view('auth');
    }

    public function registration(Request $request) {

        $validator = Validator::make($request->all(), [
            'name_registration' => 'required|min:2',
            'email_registration' => 'required|email|unique:accounts,email',
            'password_registration' => 'required|min:6',
            'repeat_password_registration' => 'required|same:password_registration',
            'agree_registration' => 'required'
        ], [
            'name_registration.required' => "Поле обов'язкове!",
            'name_registration.min' => "Ім'я повинне бути не меньше 2 символів!",
            'email_registration.required' => "Поле обов'язкове!",
            'email_registration.email' => 'Неправильно заповнене поле!',
            'email_registration.unique' => 'Ця пошта вже зайнята!',
            'password_registration.required' => "Поле обов'язкове!",
            'password_registration.min' => "Пароль повинен бути не меньше 6 символів!",
            'repeat_password_registration.required' => "Поле обов'язкове!",
            'repeat_password_registration.same' => 'Паролі не співпадають!',
            'agree_registration.required' => "Поле обов'язкове!"
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $account = new Account();
        $account->name = $request->name_registration;
        $account->email = $request->email_registration;
        $account->is_first = 1;
        $password = $request->password_registration;
        $account->password = bcrypt($password);

        $account->save();

        auth('account')->login($account);

        Mail::send('mail.registration', ['user' => $account, 'password' => $password], function($message) use($account)
        {
            $message->from('test@test.com');
            $message->to($account->email)->subject('Регистрация на Cultfood');
        });

        return redirect()->route('account.home');

    }

    public function login(Request $request) {


        $validator = Validator::make($request->all(), [
            'email_login' => 'required|email|exists:accounts,email',
            'password_login' => 'required|min:6'
        ], [
            'email_login.required' => "Поле обов'язкове!",
            'email_login.email' => 'Неправильно заповнене поле!',
            'email.exists' => 'Користувача с таким email не існує!',
            'password_login.required' => "Поле обов'язкове!",
            'password_login.min' => "Неправильний пароль!"
        ]);


        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        if (auth('account')->attempt(['email' => $request->email_login, 'password' => $request->password_login])) {
            if(!auth('account')->user()->is_first) {
                $user = auth('account')->user();
                $user->is_first = 1;
                $user->save();

                return redirect()->route('account.home');
            }
            else {
                return redirect()->route('account.instruction');
            }

        }
        else {
            $passwordError = 'Неправильний пароль!';
            return redirect()->back()->withErrors(['password_login' => $passwordError])->withInput();
        }

    }

    public function logout() {

        auth('account')->logout();
        return redirect()->route('auth');
    }


    public function redirectToProvider($driver) {
        return Socialite::driver($driver)->redirect();
    }

    public function handleProviderCallback($driver)
    {
        $user = Socialite::driver($driver)->user();

        if($user) {

            $account = Account::where($driver.'_id', $user->id)->first();

            if(!$account) {

                if(isset($user->email)) {
                    $isAccountEmail = Account::where('email', $user->email)->first();

                    if($isAccountEmail) {
                        $isAccountEmail->{$driver.'_id'} = $user->id;
                        $isAccountEmail->save();

                        auth('account')->login($isAccountEmail);
                        if($isAccountEmail->is_first) {
                            return redirect()->route('account.instruction');
                        }
                        else {
                            return redirect()->route('account.home');
                        }

                    }

                }


                $account = new Account;
                $account->{$driver.'_id'} = $user->id;
                $nameAccount = explode(' ', $user->name);
                $account->name = $nameAccount[0];
                $account->last_name = $nameAccount[1];

                if(isset($user->email)) {
                    $account->email = $user->email;
                }

                $user->is_first = 1;
                $account->save();

                auth('account')->login($account);
                return redirect()->route('account.home');
            }

            auth('account')->login($account);

            if($account->is_first) {
                return redirect()->route('account.instruction');
            }
            else {
                return redirect()->route('account.home');
            }

        }


    }


}
