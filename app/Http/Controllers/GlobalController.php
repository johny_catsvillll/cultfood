<?php

namespace App\Http\Controllers;

use App\Exports\Account;
use App\Shortcode;
use Excel;
use App\Page;
use Illuminate\Http\Request;
use LaravelAdminPanel\Models\Setting;

class GlobalController extends Controller
{
    public function counterDownloadPdf() {

        $counter = Setting::where('key', 'site.download_pdf_file')->first();

        $counter->value++;
        $counter->save();

    }

    public function saveCookie() {

        session(['cookie' => 1]);

    }

    public function page(Request $request, $page) {
        $page = Page::where('slug', $page)->where('active', 1)->firstOrFail();

        return view('page', $page);
    }

    public function export(Request $request)
    {
        $dateFrom = $request->input('dateFrom');
        $dateTo = $request->input('dateTo');
        return Excel::download(new Account($dateFrom, $dateTo), 'account.xls');
    }

    public function saveShortcodes(Request $request) {

        if($request->update) {
            $shortCode =Shortcode::find($request->id);
        }
        else {
            $shortCode = new Shortcode;
            $shortCode->news_id = $request->id;
        }

        $shortCode->text = $request->text;
        $shortCode->save();

        $view = view('blocks.shortcode-admin', compact('shortCode'))->render();

        return $view;
    }

    public function removeShortcodes(Request $request) {
        $shortCode = Shortcode::find($request->id);
        $shortCode->delete();

    }
}
