<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function faq() {

        $questions = Question::all();

        return view('faq', compact('questions'));
    }
}
