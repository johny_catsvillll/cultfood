<?php

namespace App\Http\Controllers;

use App\Account;
use App\Achievement;
use App\News;
use App\Partner;
use App\Photo;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {

        $achievements = Achievement::orderBy('order')->get();

        $news = News::orderBy('date_publication', 'desc')
            ->where('date_publication', '<', now())
            ->where('active', 1)->paginate(4);

        $photos = Photo::take(4)->where('active', 1)->orderBy('order')->get();

        $partners = Partner::all();

        return view('home', compact('achievements', 'news', 'photos', 'partners'));
    }
}
