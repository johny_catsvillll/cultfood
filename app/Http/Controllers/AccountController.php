<?php

namespace App\Http\Controllers;

use App\Mail as MailModel;
use App\Region;
use App\Step;
use Illuminate\Http\Request;
use Validator;
use Mail;

class AccountController extends Controller
{
    public function home(Request $request) {
        $user = auth('account')->user();

        if($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:2',
                'password' => 'required|min:6',
                'repeat_password' => 'required|same:password',
            ], [
                'name_registration.required' => "Поле обов'язкове!",
                'name_registration.min' => "Ім'я повинне бути не меньше 2 символів!",
                'password.required' => "Поле обов'язкове!",
                'password.min' => "Пароль повинен бути не меньше 6 символів!",
                'repeat_password.required' => "Поле обов'язкове!",
                'repeat_password.same' => 'Паролі не співпадають!',
            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            $user->name = $request->name;
            $user->last_name = $request->last_name;
            $password = $request->password;
            $user->password = bcrypt($password);

            $user->save();

            Mail::send('mail.change-data', ['user' => $user, 'password' => $password], function($message) use($user)
            {
                $message->from('test@test.com');
                $message->to($user->email)->subject('Зміна данних на Cultfood');
            });

            return redirect()->back()->with('message', 'Інформація успішно оновлена!');


        }


        return view('account.setting', compact('user'));
    }

    public function instruction(Request $request) {

        $steps = Step::orderBy('order')->get();

        return view('account.instruction', compact('steps'));
    }

    public function liders(Request $request) {

        $regions = Region::has('liders', '>=', 1)->get();

        return view('account.liders', compact('regions'));
    }

    public function chat(Request $request) {

        return view('account.chat');
    }

    public function saveMessage(Request $request) {

        $mail = new MailModel;
        $mail->mail = $request->message;
        $mail->account_id = auth('account')->user()->id;
        $mail->save();

        return redirect()->back()->with('message', 'Повідомлення успішно відправлено!');

    }

    public function saveCurrentArea(Request $request) {

        $user = auth('account')->user();
        $user->region_id = $request->id;
        $user->save();

    }

    public function saveStep(Request $request) {
        $user = auth('account')->user();
        $user->step = $request->step+1;
        $user->save();
    }
}
