<?php

namespace App\Http\Controllers;

use App\Otg;
use App\Region;
use Illuminate\Http\Request;

class OtgController extends Controller
{
    public function otg() {
        $regions = Region::has('liders', '>=', 1)->get();

        return view('otg', compact('regions'));
    }

    public function showOtg(Request $request) {

        $otgs = Otg::where('region_id', $request->id)->where('active', 1)->get();
        $view = view('blocks.list-otg', compact('otgs'))->render();

        return ['html' => $view];

    }
}
