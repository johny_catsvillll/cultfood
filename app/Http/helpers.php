<?php

function getMonth($date) {

    $date = strtotime($date);
    $month = date('m', $date);

    $months = [
        '01' => 'Cічень',
        '02' => 'Лютий',
        '03' => 'Березень',
        '04' => 'Квітень',
        '05' => 'Травень',
        '06' => 'Червень',
        '07' => 'Липень',
        '08' => 'Серпень',
        '09' => 'Вересень',
        '10' => 'Жовтень',
        '11' => 'Листопад',
        '12' =>'Грудень'
    ];

    return $months[$month];

}


function selectData($data, $nameField) {

    $value = '';

    if(old($nameField)) {
        $value = old($nameField);
    }

    if($data && $data->{$nameField}) {
        $value = $data->{$nameField};
    }

    return $value;

}


function renderShortCodes($text)
{
$shortCodes = searchShortCodes($text);
        $codes = [];
        $htmls = [];
        foreach ($shortCodes as $key => $code) {
            $tag = $code['tag'];
            $template = getShortCodeTemplate($tag);
            $data = getShortCodeData($tag, $code['id']);
            if (!$data){
                $codes[] = $code['code'];
                $htmls[] = '';
                continue;
            }
            $codes[] = $code['code'];
            $htmls[] = (string) view($template, ['data' => $data]);
        }
        $output = str_replace($codes, $htmls, $text);
        return $output;
    }


function searchShortCodes($text)
{
    $pattern = '#\[([-_a-z0-9]{1,40})\]((\d){1,15})\[/\1\]#';
    $matches = [];
    $shortCodes = [];
    $r = preg_match_all($pattern, $text, $matches);
    foreach ($matches[2] as $index => $code_id) {
        $shortCodes[] = [
            'id'   => $code_id,
            'tag'  => $matches[1][$index],
            'code' => $matches[0][$index],
        ];
    }
    return $shortCodes;
}


function getShortCodeTemplate($tag) {
    switch($tag){
        case 'quote' :
            $template = 'shortcodes.quote';
            break;
        default :
            $template = null;
    }
    return $template;
}

function getShortCodeData($code_tag, $code_id)
{
    switch($code_tag){
        case 'quote' :
            $data = getQuoteData($code_id);
            break;
        default :
            $data = null;
    }
    return $data;
}

function getQuoteData($quote_id)
{
    $block = \App\Shortcode::find($quote_id);
    if (!$block) return null;
    return [
        'id' => $block->id,
        'text' => $block->text,
    ];
}