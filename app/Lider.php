<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAdminPanel\Traits\Cropper;


class Lider extends Model
{

    use Cropper;

    public function regionId() {
        return $this->belongsTo(Region::class, 'region_id');
    }
}
