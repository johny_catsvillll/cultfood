<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAdminPanel\Traits\Cropper;

class News extends Model
{
    use Cropper;

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getCategoryAttribute($value) {
        $categorySlug = '';
        switch($this->category_id) {
            case 1:
                $categorySlug = 'news';
                break;
            case 2:
                $categorySlug = 'blog';
                break;
        }

        return $categorySlug;
    }

    public function save(array $options = []) {
        $tags = request()->tags;
        $this->tags()->sync($tags);


        parent::save();
    }
}
