$(document).ready(function () {
   $('.btn-hamburger').click(function (e) {
       e.preventDefault();
       $('.global-overlay').fadeIn();
       var menu = $('.modal-menu');
       menu.addClass('active');
   });

   $('.modal-menu .close-popup').click(function (e) {
       e.preventDefault();

       $('.global-overlay').fadeOut();
       var menu = $('.modal-menu');
       menu.removeClass('active');

   });

   $('.button-cookie').click(function () {
       $('#cookie').hide();
   });

   $('.upload-compilation').click(function () {
        $.ajax({
            type: 'get',
            url: '/counter-download-pdf'
        });
   });

   $('.button-cookie').click(function () {
       $.ajax({
           type: 'get',
           url: '/save-cookie'
       });
   });

   $('.wrap-list-area ul li:first-child').click(function () {

       var el = $(this).closest('ul');
       if(el.hasClass('active')) {
           el.removeClass('active');
       }
       else {
           el.addClass('active');

       }
   });

});