(function( $ ){

    $.fn.filemanager = function(type, options) {
        type = type || 'file';

        this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).data('input'));
            localStorage.setItem('target_preview', $(this).data('preview'));
            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            window.SetUrl = function (url, file_path) {
                //set the value of the desired input to image url
                var target_input = $('#' + localStorage.getItem('target_input'));
                target_input.val(file_path).trigger('change');

                //set or change the preview image src
                var target_preview = $('#' + localStorage.getItem('target_preview'));
                target_preview.attr('src', url).trigger('change');
            };
            return false;
        });
    }


})(jQuery);

$(document).ready(function () {
    $.ajaxSetup({headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")}});
    var e = {
        path_absolute: "/",
        selector: "textarea.richTextBox1",
        language: "uk",
        height: "300",
        autosave_interval: "10s",
        browser_spellcheck: !0,
        contextmenu: !1,
        plugins: ["image charmap preview anchor", "wordcount visualblocks visualchars code fullscreen", "insertdatetime nonbreaking save directionality", "emoticons template paste textcolor colorpicker textpattern youtube media autosave"],
        toolbar: "undo redo | styleselect removeformat | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink | image" +
            " youtube | anchor",
        relative_urls: !1,
        rel_list: [{title: "---", value: ""}, {title: "nofollow", value: "nofollow"}],
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName("body")[0].clientWidth,
                y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName("body")[0].clientHeight;

            var cmsURL = e.path_absolute + 'laravel-filemanager?field_name=' + field_name;

            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        },
        init_instance_callback: function (editor) {
            editor.on('dblClick', function (e) {
                if (e.target.nodeName === 'IMG') {
                    tinyMCE.activeEditor.execCommand('mceImage');
                }
            });
        }
    };
    tinymce.init(e);
});