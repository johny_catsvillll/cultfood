<div class="form-field create-shortcode">
    <input type="text" class="new-shortcode form-control" placeholder="Новий Shortcode">
    <a href="" class="btn-primary btn">Створити</a>
</div>

@php
    $shortCodes = \App\Shortcode::where('news_id', $dataTypeContent->id)->get();

@endphp

<ul style="list-style: none; padding-left: 0;" class="list-shortcodes">
    @foreach($shortCodes as $shortCode)
        @include('blocks.shortcode-admin')
    @endforeach
</ul>

<style>
    .list-shortcodes li ul li {
        display: inline-block;
        vertical-align: top;
    }
</style>

@section('javascript')

    <script>
        $('body').on('click', '.create-shortcode a', function (e) {
            e.preventDefault();

            var text = $('.new-shortcode').val();
            var id = '{{ $dataTypeContent->id }}';

            $.ajax({
                type: 'get',
                url: '/save-shortcode',
                data: {text:text, id:id},
                success:function (result) {
                    $('.list-shortcodes').append(result);
                }
            });

        });

        $('body').on('click', '.list-shortcodes .btn-danger', function (e) {
            e.preventDefault();
            var el = $(this);
            var id = el.closest('ul').closest('li').data('id');

            $.ajax({
                type: 'get',
                url: '/remove-shortcode',
                data: {id:id},
                success:function (result) {
                    el.closest('ul').closest('li').remove();
                }
            });

        });

        $('body').on('click', '.list-shortcodes .btn-primary', function (e) {
            e.preventDefault();
            var el = $(this);
            var input = el.closest('ul').closest('li').find('input.quote-text');
            input.val(el.closest('ul').closest('li').data('text'));
            input.attr('readonly', false);
            el.closest('ul').closest('li').find('.btn-success').removeClass('hide');
        });

        $('body').on('click', '.list-shortcodes .btn-success', function (e) {
            e.preventDefault();
            var el = $(this);
            var id = el.closest('ul').closest('li').data('id');
            var text = el.closest('ul').closest('li').find('input.quote-text').val();

            $.ajax({
                type: 'get',
                url: '/save-shortcode',
                data: {text:text, id:id, update:true},
                success:function (result) {
                    el.closest('ul').closest('li').replaceWith(result);
                }
            });

        });

    </script>
    @parent
@endsection