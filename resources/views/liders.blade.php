@extends('layout')

@section('content')
    <section id="slideFaq" class="slider-liders">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $seo->caption }}</h2>
                    <span>{{ $seo->description }}</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>ЛІДЕРИ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="liders">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 order-12 order-last order-xs-last order-lg-first">
                    <div class="wrap-list-area">
                        <ul>
                            <li>ОБЕРІТЬ ОБЛАСТЬ</li>
                            @foreach($regions as $region)
                                <li><a href="" data-id="{{ $region->id }}">{{ $region->region }} область</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-8 order-first order-1 offset-lg-1 order-xs-2 order-lg-last">
                    <div class="content-page">
                        <p> {{ html_entity_decode(strip_tags($seo->body)) }} </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="listLiders">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 offset-lg-3">
                    <ul></ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('.wrap-list-area ul li a').click(function (e) {
                $('.wrap-list-area ul li a').removeClass('active');
                e.preventDefault();
                var el = $(this);
                el.addClass('active');
                var id = el.data('id');
                $('#listLiders ul').empty();
                $.ajax({
                    type: 'get',
                    url: '/show-liders',
                    data: {id:id},
                    success:function (result) {
                        $('#listLiders ul').append(result.html);
                    }
                });

            })
        });
    </script>
@endsection