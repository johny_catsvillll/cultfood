@foreach($liders as $lider)
    <li>
        <img src="{{ $lider->getCroppedPhoto('image', 'avatar', 'max') }}" alt="">
        <span class="name-lider">{{ $lider->name }}</span>
        <span class="area-lider">{{ $lider->regionId->region }}</span>

        <ul>
            <li><span>Телефон:</span> <a href="">{{ $lider->phone }}</a></li>
            <li><span>E-mail:</span> <a href="mailto:{{ $lider->email }}">{{ $lider->email }}</a></li>
        </ul>
    </li>
@endforeach