@foreach($news as $new)
    <div class="col-lg-5 col-sm-6 @if($loop->iteration % 2 === 0) bounceInRight @else offset-lg-1 bounceInLeft @endif animated">
        <div class="wrap-img">
            <div class="string-bottom"></div>
            <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}">
                @if(strpos($new->image, 'wp-content') === false && strpos($new->image, 'post') === false && strpos($new->image, 'page') === false)
                    <img src="{{ $new->getCroppedPhoto('image', 'list', 'photo')}}" alt="">
                @else
                    <img style="height: 92%" src="{{ $new->image }}" alt="">
                @endif
            </a>
        </div>
        <div class="wrap-content">
            <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}" class="title">{{ $new->post_title }}</a>
            <span class="date">{{ getMonth($new->created_at) }} {{ date('d', strtotime($new->created_at)) }}, {{ date('Y', strtotime($new->created_at)) }}</span>
            <p>{{ $new->post_excerpt }}</p>
            <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}" class="btn-more">Читати</a>
        </div>
    </div>
@endforeach