<div class="col-lg-3 col-md-5">
    <div class="list-categories-news">
        <ul>
            <li><a href="{{ url('parents') }}">БАТЬКАМ</a></li>
            <li><a href="{{ url('enlighten') }}">ОСВІТЯНАМ</a></li>
            <li><a href="{{ url('cooks') }}">КУХАРЯМ</a></li>
        </ul>
    </div>

    <div class="siderbar-news">
        <h3>Новини Проекту</h3>
        <ul>
            @foreach($articles as $new)
                <li>
                    <ul>
                        <li>
                            <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}">
                                @if(strpos($new->image, 'wp-content') === false && strpos($new->image, 'post') === false && strpos($new->image, 'page') === false)
                                    <img src="{{ $new->getCroppedPhoto('image', 'photo', 'little')}}" alt="">
                                @else
                                    <img style="height: 92%" src="{{ $new->image }}" alt="">
                                @endif
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}">{{ $new->post_title }}</a>
                            <span>{{ getMonth($new->created_at) }} {{ date('d', strtotime($new->created_at)) }}, {{ date('Y', strtotime($new->created_at)) }}</span>
                        </li>
                    </ul>
                </li>
            @endforeach

        </ul>
    </div>

    <div class="help">
        <div class="overlay"></div>
        <span>Потрібна допомога?</span>
        <a href="{{ url('liders') }}">ЗВЕРНИСЬ ДО ЛІДЕРА</a>
    </div>

    <div class="socials">
        <h3>Ми в Соцмережах</h3>
        <img src="{{ asset('images/fb.jpg') }}" alt="">
    </div>
</div>