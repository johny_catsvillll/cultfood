<li data-id="{{ $shortCode->id }}" data-text="{{ $shortCode->text }}">
    <p style="width:49%;display: inline-block;"><input class="form-control quote-text" type="text" readonly="" value="{{ $shortCode->text }}"></p>
    <p style="width:49%;display: inline-block;"><input class="form-control" type="text" readonly="" value="{{ '[quote]'. $shortCode->id .'[/quote]' }}"></p>
    <ul style="list-style: none; padding-left: 0;">
        <li><a href="" class="btn btn-success hide">Зберігти</a></li>
        <li><a href="" class="btn-primary btn">Редагувати</a></li>
        <li><a href="" class="btn btn-danger">Видалити</a></li>
    </ul>
</li>