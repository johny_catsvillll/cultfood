@foreach($otgs as $otg)
    <li>
        <h4>{{ $otg->name }}</h4>
        <ul>
            <li><span>Індекс:</span> {{ $otg->index }}</li>
            <li><span>Область:</span> {{ $otg->regionId->region }}</li>
            <li><span>Район:</span> {{ $otg->area }}</li>
            <li><span>Село/Місто/Смт:</span> {{ $otg->city }}</li>
            <li><span>Вулиця:</span> {{ $otg->city }}</li>
            <li><span>Будинок:</span> {{ $otg->house }}</li>
            <li><span>E-mail:</span> {{ $otg->email }}</li>
            <li><span>Телефон:</span>{{ $otg->phone }}</li>
        </ul>
    </li>
@endforeach
