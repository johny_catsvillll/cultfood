@foreach($news as $new)
    <li>
        <div class="wrap-news">
            <div class="news-img">
                <div class="string-right"></div>

                <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}">
                    @if(strpos($new->image, 'wp-content') === false && strpos($new->image, 'post') === false && strpos($new->image, 'page') === false)
                        <img src="{{ $new->getCroppedPhoto('image', 'list', 'photo')}}" alt="">
                    @else
                        <img style="height: 92%" src="{{ $new->image }}" alt="">
                    @endif
                </a>
            </div>
            <div class="content-news">
                <a class="name-news" href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}">{{ $new->post_title }}</a>
                <span>{{ getMonth($new->date_publication) }} {{ date('d', strtotime($new->date_publication)) }}, {{ date('Y', strtotime($new->date_publication)) }}</span>
                <p>{{ $new->post_excerpt }}</p>

                <a href="{{ route('one-'.$new->category, ['id' => $new->id, 'slug' => $new->slug]) }}" class="read-more">Читати</a>
            </div>
        </div>
    </li>
@endforeach