@extends('layout')

@section('content')
    <section id="slideFaq">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Особистий кабінет</h2>
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <br>
Aenean commodo ligula eget dolor</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>ОСОБИСТИЙ КАБІНЕТ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="auth">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="excerpt-setting">{!! $seo ? $seo->body : '' !!}</p>
                    <ul class="list-btn-auth">
                        <li><a href="" @if(!old('registration')) class="active" @endif data-tab="login">ВХІД</a></li>
                        <li><a href="" @if(old('registration')) class="active" @endif data-tab="registration">РЕЄСТРАЦІЯ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="login" @if(!old('registration')) class="active" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4>Вхід</h4>
                    <form action="{{ url('login') }}" method="post">
                        {{ csrf_field() }}

                        <div class="wrap-auth-field">
                            <label for="">
                                Введіть свій e-mail

                                @if($errors->first('email_login'))
                                    <span class="error">
                                        {{ $errors->first('email_login') }}
                                    </span>
                                @endif

                            </label>
                            <input type="email" name="email_login" value="{{ old('email_login') }}">
                        </div>

                        <div class="wrap-auth-field">
                            <label for="">
                                Введіть пароль
                                @if($errors->first('password_login'))
                                    <span class="error">
                                        {{ $errors->first('password_login') }}
                                    </span>
                                @endif
                            </label>
                            <input type="password" name="password_login">
                        </div>

                        <ul class="list-current-btn-auth">
                            <li>
                                <input type="submit" name="submit" value="УВІЙТИ">
                            </li>
                            <li>
                                <a href="">Забули пароль?</a>
                            </li>
                        </ul>

                        <ul class="btn-social">
                            <li><a href="{{ url('oauth/google') }}">УВІЙТИ ЧЕРЕЗ GOOGLE</a></li>
                            <li><a href="{{ url('oauth/facebook') }}">УВІЙТИ ЧЕРЕЗ FACEBOOK</a></li>
                        </ul>

                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="registration" @if(old('registration')) class="active" @endif>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4>Реєстрація</h4>
                    <form action="{{ url('registration') }}" method="post">
                        <input type="hidden" name="registration" value="1">
                        {{ csrf_field() }}
                        <div class="wrap-auth-field">
                            <label for="">
                                Ваше ім’я
                                @if($errors->first('name_registration'))
                                    <span class="error">
                                        {{ $errors->first('name_registration') }}
                                    </span>
                                @endif
                            </label>
                            <input type="text" name="name_registration" value="{{ old('name_registration') }}">
                        </div>

                        <div class="wrap-auth-field">
                            <label for="">
                                Ваш e-mail
                                @if($errors->first('email_registration'))
                                    <span class="error">
                                        {{ $errors->first('email_registration') }}
                                    </span>
                                @endif
                            </label>
                            <input type="email" name="email_registration" value="{{ old('email_registration') }}">
                        </div>

                        <div class="wrap-auth-field">
                            <label for="">
                                Введіть пароль
                                @if($errors->first('password_registration'))
                                    <span class="error">
                                        {{ $errors->first('password_registration') }}
                                    </span>
                                @endif
                            </label>
                            <input type="password" name="password_registration">
                        </div>

                        <div class="wrap-auth-field">
                            <label for="">
                                Підтвердження паролю
                                @if($errors->first('repeat_password_registration'))
                                    <span class="error">
                                        {{ $errors->first('repeat_password_registration') }}
                                    </span>
                                @endif
                            </label>
                            <input type="password" name="repeat_password_registration">
                        </div>

                        <div class="agree-wrap">
                            <input type="checkbox" name="agree_registration" id="agree" @if(old('agree_registration')) checked @endif>
                            <label for="agree">Создавая аккаунт, я принимаю условия, изложенные в документах «Условия пользования веб-сайтом», <br>
                                «Политика конфиденциальности» и «Условия лицензирования»</label>
                            @if($errors->first('agree_registration'))
                                <span class="d-block error">
                                        {{ $errors->first('agree_registration') }}
                                    </span>
                            @endif
                        </div>

                        <input type="submit" name="submit" value="ЗАРЕЄСТРУВАТИСЯ">


                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('.list-btn-auth li a').click(function (e) {
                e.preventDefault();
                var el = $(this);
                var tab = el.data('tab');
                $('#login, #registration').hide();
                $('.list-btn-auth li a').removeClass('active');
                $('#' + tab).show();
                el.addClass('active');
                $('#auth').css({marginBottom: 0});
            });


        });
    </script>
@endsection