@extends('layout')

@section('content')
    <section id="breadcrumbs" class="border-top-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>СТОРІНКА НЕ ЗНАЙДЕНА</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="page404">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img src="{{ asset('images/404.jpg') }}" alt="">
                    <h3>Сталася помилка!</h3>
                    <p>Адресу набрано неправильно, або ця сторінка на сайті більше не існує.</p>
                    <a href="{{ url('/') }}">НА ГОЛОВНУ</a>
                </div>
            </div>
        </div>
    </section>

@endsection