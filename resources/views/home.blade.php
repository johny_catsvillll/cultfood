@extends('layout')

@section('content')


    <section id="categories">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="list-categories">
                        <ul>
                            <li><a href="{{ url('parents') }}">Батькам
                                    <img src="{{ asset('../images/category-1.png') }}" alt="">
                                </a></li>
                            <li><a href="{{ url('enlighten') }}">Освітянам
                                    <img src="{{ asset('../images/category-2.png') }}" alt="">
                                </a></li>
                            <li><a href="{{ url('cooks') }}">Кухарям
                                    <img src="{{ asset('../images/category-3.png') }}" alt="">
                                </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="slider" style="background-image: url(/storage/{{ $seo->image }})">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="content-slide">
                        <h2>{{ $seo->caption }}</h2>
                        <span>{{ $seo->description }}</b>. </span>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="progress">
        <div class="container">
            <div class="row">
                <div class="offset-lg-1 col-lg-5 col-md-6 col-sm-6 left-progress bounce animated">
                            <span class="text-progress">Наші Досягнення
                                <img src="{{ asset('images/progress-image.png') }}" alt="">
                            </span>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-4 right-progress bounce animated">
                    <ul class="list-progress-categories">
                        @foreach($achievements as $achievement)
                            <li><a href="" @if($loop->first) class="active" @endif
                                data-text="{{str_replace($achievement->number, '<br><span>'.$achievement->number.'</span>', $achievement->text)}}">{{ $achievement->name
                                 }}</a></li>
                        @endforeach
                    </ul>

                    <p class="text-two-progress">
                            {!! str_replace($achievements[0]->number, '<br><span>'.$achievements[0]->number.'</span>', $achievements[0]->text) !!}
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="news">
        <h2>Новини проекту</h2>
        <div class="container">
            <div class="row">
                    @include('blocks.list-articles')

                    <input type="hidden" name="page" value="2">

            </div>
            <a href="" class="show-more">Показати ще</a>
        </div>
    </section>

    <section id="gallery">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        @foreach($photos as $photo)
                            <li>
                                <div>
                                    <img src="{{ asset('storage/'. $photo->image) }}" alt="">
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="partners">
        <h2>Партнери</h2>

        <div class="container">
            <div class="responsive" data-slick='{"slidesToShow": 5, "slidesToScroll": 1}'>
                {{--<div>--}}
                    {{--<img class="img-fluid mx-auto d-block" src="{{ asset('images/partners1.png') }}" alt="slide 1">--}}
                {{--</div>--}}
                {{--<div>--}}
                    {{--<img class="img-fluid mx-auto d-block" src="{{ asset('images/partners2.png') }}" alt="slide 1">--}}
                {{--</div>--}}
                {{--<div>--}}
                    {{--<img class="img-fluid mx-auto d-block" src="{{ asset('images/partners3.png') }}" alt="slide 1">--}}
                {{--</div>--}}
                {{--<div>--}}
                    {{--<img class="img-fluid mx-auto d-block" src="{{ asset('images/partners4.png') }}" alt="slide 1">--}}
                {{--</div>--}}
                {{--<div>--}}
                    {{--<img class="img-fluid mx-auto d-block" src="{{ asset('images/partners5.png') }}" alt="slide 1">--}}
                {{--</div>--}}
                {{--<div>--}}
                    {{--<img class="img-fluid mx-auto d-block" src="{{ asset('images/partners5.png') }}" alt="slide 1">--}}
                {{--</div>--}}

                @foreach($partners as $partner)
                    <div>
                        <img class="img-fluid mx-auto d-block" src="{{ asset('storage/'.$partner->image) }}" alt="slide 1">
                    </div>
                @endforeach

            </div>
        </div>

    </section>


@endsection

@section('javascript')
    <script>

        $('#partners .responsive').slick({
            dots: false,
            speed: 300,
            arrows:true,
            infinite:true,
            prevArrow: '<button type="button" class="slick-prev"><img src="{{ asset('images/arrow.png') }}" alt=""></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="{{ asset('images/arrow.png') }}" alt=""></button>',
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 10000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });


        var scrollValue = $('#progress').offset().top - 800;
        $(window).scroll(function () {
            var scrollValue = $('#progress').offset().top - 800;
            if ($(this).scrollTop() > scrollValue) {
                $('#progress').addClass('active');
            }
        });

        if ($(this).scrollTop() > scrollValue) {
            $('#progress').addClass('active');
        }

        $('.list-progress-categories li a').click(function (e) {
            e.preventDefault();
            $('.list-progress-categories li a').removeClass('active');
            var el = $(this);
            el.addClass('active');
            var text = el.data('text');

            console.log(text);
            $('.text-two-progress').html(text);

        });

        $('.show-more').click(function (e) {
            e.preventDefault();
            var page = $('input[name="page"]');
            var el = $(this);

            $.ajax({
                type: 'get',
                url: '/show-articles',
                data: {page:page.val(), type: 'articles'},
                success:function (result) {
                    $('#news .row').append(result.html);

                    if(page.val() == result.lastPage || page.val() > result.lastPage) {
                        el.remove();
                    }

                    page.val(+page.val() + 1);
                }
            });

        });

    </script>

@endsection