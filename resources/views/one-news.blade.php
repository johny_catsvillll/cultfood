@extends('layout')

@section('robots')
    @if($news->date_publication < now())
        <meta name="robots" content="noindex, nofollow" />
    @endif
@endsection


@section('seo')

    <title>{{ $news->meta_title }}</title>
    <meta name="keywords" content="{{ $news->meta_keywords }}">
    <meta name="description" content="{{ $news->meta_description }}">


    <meta property="og:url" content="{{ url(Request::path()) }}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $news->post_title }}">
    <meta property="og:description" content="{{ $news->meta_description }}">
    <meta property="og:image:type" content="image/jpg"/>
    <meta property="og:image" content="{{ url($news->getCroppedPhoto('og_image', 'facebook', 'max')) }}">
    <meta property="og:image:width" content="810"/>
    <meta property="og:image:height" content="600"/>

    <link rel="canonical" href="{{ url(Request::path()) }}"/>
@endsection

@section('content')
    <section id="slideFaq" class="news-page">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $news->post_title }}</h2>
                    <span>{{ $news->post_excerpt }}</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        @if($news->category_id == 1)
                            <li><a href="{{ route('news') }}">Новини проекту</a></li>
                        @else
                            <li><a href="{{ url('blog') }}">Блог</a></li>
                        @endif
                        <li class="mobile-display-none"><a>|</a></li>
                        <li class="mobile-display-none"><a>{{ $news->post_title }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="news" class="one-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-7">
                    <img src="{{ $news->getCroppedPhoto('image', 'image', 'big') }}" alt="">
                    <div class="body-news">
                        <h1>{{ $news->name }}</h1>
                        <span class="date-news">{{ getMonth($news->date_publication) }} {{ date('d', strtotime($news->date_publication)) }},
                            {{ date('Y', strtotime($news->date_publication)) }}</span>
                        {!! $news->post_content !!}

                    </div>

                    <div class="share">
                        <ul>
                            <li>Поділитися:</li>
                            <li><a href="https://www.facebook.com/sharer.php?u={{ route('one-news', ['id' => $news->id, 'slug' => $news->slug]) }}" class="share-icon fb">
                                    <img src="{{ asset('images/icons/share-fb.png') }}" alt="">
                                </a></li>
                            <li><a href="https://twitter.com/share" class="share-icon tw">
                                    <img src="{{ asset('images/icons/share-twitter.png') }}" alt="">
                                </a></li>
                            {{--<li><a href="" class="share-icon tg">--}}
                                    {{--<img src="{{ asset('images/icons/share-telegram.png') }}" alt="">--}}
                                {{--</a></li>--}}
                            {{--<li><a href="" class="share-icon viber">--}}
                                    {{--<img src="{{ asset('images/icons/share-phone.png') }}" alt="">--}}
                                {{--</a></li>--}}
                        </ul>
                    </div>


                    <section id="lastNews">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="last-news">
                                        <ul>
                                            <li>
                                                <span>ПОПЕРЕДНІЙ ПОСТ</span>
                                                <a href="{{ route('one-news', ['id' => $prevNews->id, 'slug' => $prevNews->slug]) }}">{{ $prevNews->post_title }}</a>
                                            </li>
                                            <li>
                                                <img src="{{ asset('images/kisspng_asparagus_organic_food_vegetarian.png') }}" alt="">
                                            </li>
                                            <li>
                                                <span>НАСТУПНИЙ ПОСТ</span>
                                                <a href="{{ route('one-news', ['id' => $nextNews->id, 'slug' => $nextNews->slug]) }}">{{ $nextNews->post_title }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                @include('blocks.sidebar')
            </div>
        </div>
    </section>


@endsection