<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--<link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">

    @section('seo')

        <title>{{ isset($seo) && $seo ? $seo->meta_title : '' }}</title>
        <meta name="keywords" content="{{ isset($seo) && $seo ? $seo->meta_keywords : '' }}">
        <meta name="description" content="{{ isset($seo) && $seo ? $seo->meta_description : '' }}">

        <link rel="canonical" href="{{ url(Request::path()) }}"/>
    @endsection

    @yield('seo')

    @section('robots')
        <meta name="robots" content="noindex, nofollow" />
    @endsection

    @yield('robots')

</head>
<body>

<div id="wrapper">
    <section id="header">
        <div class="container">
            <div class="row">
                <div class="col-3 col-sm-4 col-xs-2 col-lg-4">
                    <a href="{{ url(json_decode(setting('site.pdf_file'))[0]->download_link) }}" download="Збірник Cultfood" class="d-block upload-compilation
                    animated"><span>Завантажити
                            збірник</span></a>
                </div>
                <div class="col-6 col-md-5 col-sm-3 col-xs-3 col-lg-4">
                    <a href="{{ url('/') }}" class="logo">
                        <img src="{{ asset('images/logo.png') }}" alt="">
                    </a>
                </div>
                <div class="col-3 offset-lg-2 col-lg-2 col-sm-3">

                    <a href="{{ auth('account')->user() ? url('account') : route('auth') }}" class="btn-auth">
                        <img src="{{ asset('images/icons/user.png') }}" alt="">
                    </a>

                </div>
            </div>
        </div>
    </section>

    <section id="menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li>
                            <a href="" class="btn-hamburger">
                                <img src="{{ asset('images/icons/hamburger.png') }}" alt="">
                                <span>Меню</span>
                            </a></li>
                        @foreach($mainMenu as $m)
                            <li><a @if(Request::path() == $m->pageId->slug) class="active" @endif href="{{ url($m->pageId->slug) }}" >{{ $m->name }}</a></li>
                        @endforeach
                        <li><a href="">
                                <img src="{{ asset('images/icons/search.png') }}" alt="">
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @yield('content')

</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/slick.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>

@yield('javascript')

<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="content-footer">
                    <a href="{{ url('/') }}" class="logo-footer">
                        <img src="{{ asset('images/logo-footer.png') }}" alt="">
                    </a>
                    <span>Нове шкільне харчування </span>

                    <a href="{{ url('contacts') }}" class="write-us">Написати нам</a>

                    <p>Слідкуйте за нами</p>

                    <ul>
                        <li><a href="https://www.facebook.com/nsf.cult.food"><img src="{{ asset('images/icons/fb.png') }}" alt=""></a></li>
                        <li><a href="https://bit.ly/2NTVJgC?fbclid=IwAR1PGVRDgSt2JgKHCCBMNEcUTiMXz3DiJDTRtYid4PjQ5nifM95KEyNw9tI"><img src="{{ asset('images/icons/phone.png') }}" alt=""></a></li>
                        <li><a href="https://www.youtube.com/channel/UCTlLHk2fzG8Jzv0ipeXKzxA"><img src="{{ asset('images/icons/yt.png') }}" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="global-overlay"></div>
<div class="modal-menu">
    <a href="" class="close-popup">
        <img src="{{ asset('images/icons/close-popup.png') }}" alt="">
    </a>

    <a class="logo-popup" href="{{ url('/') }}">
        <img src="{{ asset('images/logo-mobile.png')  }}" alt="">
    </a>


    <div class="list-menu-popup">
        <ul>
            @foreach($mainMenu as $m)
                <li><a href="{{ url($m->pageId->slug) }}">{{ $m->name }}</a></li>
            @endforeach
            <li><a href="{{ url('parents') }}" class="border-none">БАТЬКАМ</a></li>
            <li><a href="{{ url('enlighten') }}" class="border-none">ОСВІТЯНАМ</a></li>
            <li><a href="{{ url('cooks') }}" class="border-none">КУХАРЯМ</a></li>
        </ul>
    </div>

    <div class="list-social-popup">
        <ul>
            <li><a href="https://www.facebook.com/nsf.cult.food">
                    <img src="{{ asset('images/icons/fb.png') }}" alt="">
                </a></li>
            <li><a href="https://bit.ly/2NTVJgC?fbclid=IwAR1PGVRDgSt2JgKHCCBMNEcUTiMXz3DiJDTRtYid4PjQ5nifM95KEyNw9tI">
                    <img src="{{ asset('images/icons/phone.png') }}" alt="">
                </a></li>
            <li><a href="https://www.youtube.com/channel/UCTlLHk2fzG8Jzv0ipeXKzx">
                    <img src="{{ asset('images/icons/yt.png') }}" alt="">
                </a></li>
        </ul>
    </div>

    <a href="{{ route('auth') }}" class="btn-auth-popup">УВІЙТИ</a>

</div>
@if(!session('cookie'))
    <section id="cookie">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="cookies visible" id="gdpr_cookies">
                        <div class="cookies__container">
                            <div class="cookies__container_description">
                                <p class="title-cookie">
                                    Ми оновили правила збору та зберігання персональних даних
                                </p>
                                <p class="text">
                                    Ви можете ознайомитися зі змінами в
                                    <a href="" style="color:green;">
                                        політиці конфіденційності.
                                    </a>
                                    Натискаючи на кнопку «Прийняти» або продовжуючи користуватися сайтом, ви погоджуєтеся з оновленими правилами і даєте дозвіл на використання файлів cookie.
                                </p>
                            </div>
                            <div class="cookies__container_button">
                                <p class="button-cookie">
                                    Прийняти
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
</body>
</html>