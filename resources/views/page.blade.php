@extends('layout')

@section('content')
    <section id="slideFaq" class="news-page">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $seo->name }}</h2>
                    <span>{{ $seo->description }}</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li class=""><a>{{ $seo->name }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="news" class="one-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-7">
                    <div class="body-news">
                        {!! $seo->body !!}
                    </div>




                </div>
                @include('blocks.sidebar')
            </div>
        </div>
    </section>


@endsection