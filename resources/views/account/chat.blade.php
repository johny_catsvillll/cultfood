@extends('layout')

@section('content')
    <section id="slideFaq">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Особистий кабінет</h2>
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <br>
Aenean commodo ligula eget dolor</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>ОСОБИСТИЙ КАБІНЕТ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="settings" class="padding-top-50">
        <div class="container">
            <div class="row">
                @include('account.blocks.left-menu')
                <div class="col-lg-8">

                    <div class="chat">
                        <ul>
                            {{--<li class="right-message">--}}
                                {{--<span class="category-message">Підтримка</span>--}}
                                {{--<span class="date-message">--}}
                                    {{--25.12.19  10:12--}}
                                {{--</span>--}}
                                {{--<p>--}}
                                    {{--Оберіть, будь ласка, що саме Вас цiкавить--}}
                                {{--</p>--}}
                            {{--</li>--}}

                            {{--<li class="left-message">--}}
                                {{--<span class="category-message">Ваня</span>--}}
                                {{--<span class="date-message">--}}
                                    {{--25.12.19  10:12--}}
                                {{--</span>--}}
                                {{--<p>--}}
                                    {{--Змінити кредитний ліміт по картці--}}
                                {{--</p>--}}
                            {{--</li>--}}


                            {{--<li class="right-message">--}}
                                {{--<span class="category-message">Підтримка</span>--}}
                                {{--<span class="date-message">--}}
                                    {{--25.12.19  10:12--}}
                                {{--</span>--}}
                                {{--<p>--}}
                                    {{--Оберіть, будь ласка, що саме Вас цiкавить--}}
                                {{--</p>--}}
                            {{--</li>--}}

                            {{--<li class="left-message">--}}
                                {{--<span class="category-message">Ваня</span>--}}
                                {{--<span class="date-message">--}}
                                    {{--25.12.19  10:12--}}
                                {{--</span>--}}
                                {{--<p>--}}
                                    {{--Змінити кредитний ліміт по картці--}}
                                {{--</p>--}}
                            {{--</li>--}}

                            {{--<li class="right-message">--}}
                                {{--<span class="category-message">Підтримка</span>--}}
                                {{--<span class="date-message">--}}
                                    {{--25.12.19  10:12--}}
                                {{--</span>--}}
                                {{--<p>--}}
                                    {{--Оберіть, будь ласка, що саме Вас цiкавить--}}
                                {{--</p>--}}
                            {{--</li>--}}

                            {{--<li class="left-message">--}}
                                {{--<span class="category-message">Ваня</span>--}}
                                {{--<span class="date-message">--}}
                                    {{--25.12.19  10:12--}}
                                {{--</span>--}}
                                {{--<p>--}}
                                    {{--Змінити кредитний ліміт по картці--}}
                                {{--</p>--}}
                            {{--</li>--}}



                        </ul>


                        <form action="">
                            <textarea name="message" id="" cols="30" rows="10"></textarea>
                            {{ csrf_field() }}
                            <input type="submit" name="submit" value="ВІДПРАВИТИ">
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection