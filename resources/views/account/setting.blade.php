@extends('layout')

@section('content')
    <section id="slideFaq">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $seo->caption }}</h2>
                    <span>{{ $seo->description }}</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>ОСОБИСТИЙ КАБІНЕТ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="settings">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="excerpt-setting">
                        {!! strip_tags($seo->body) !!}
                    </p>
                </div>
                @include('account.blocks.left-menu')
                <div class="col-lg-8">
                    <form action="{{ route('account.home') }}" method="post">
                        {{ csrf_field() }}
                        <div class="wrap-form-field">
                            <label for="">
                                <p>{{ session('message') }}</p>
                                <span>Iм’я*</span>
                                @if($errors->first('name'))
                                    <span class="error">
                                        {{ $errors->first('name') }}
                                    </span>
                                @endif
                            </label>
                            <input type="text" name="name" value="{{ selectData($user, 'name') }}">
                        </div>

                        <div class="wrap-form-field">
                            <label for="">
                                <span>Прiзвище</span>
                                @if($errors->first('last_name'))
                                    <span class="error">
                                        {{ $errors->first('last_name') }}
                                    </span>
                                @endif
                            </label>
                            <input type="text" name="last_name" value="{{ selectData($user, 'last_name') }}">
                        </div>

                        <div class="wrap-form-field">
                            <label for="">
                                <span>E-mail*</span>
                                @if($errors->first('email'))
                                    <span class="error">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </label>
                            <input type="email" readonly name="email" value="{{ selectData($user, 'email') }}">
                        </div>

                        <div class="wrap-form-field wrap-form-field-password">
                            <label for="">
                                <span>Змінити пароль*</span>
                                @if($errors->first('password'))
                                    <span class="error">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </label>
                            <input type="password" name="password" value="">
                            <a href=""></a>
                        </div>

                        <div class="wrap-form-field wrap-form-field-password">
                            <label for="">
                                <span>Підтвердити пароль</span>
                                @if($errors->first('repeat_password'))
                                    <span class="error">
                                        {{ $errors->first('repeat_password') }}
                                    </span>
                                @endif
                            </label>
                            <input type="password" name="repeat_password" value="">
                            <a href=""></a>
                        </div>

                        <input type="submit" name="submit" value="ЗБЕРЕГТИ">

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        $(".wrap-form-field-password a").click(function(e) {
            e.preventDefault();

            $(this).toggleClass("active");
            var input = $(this).closest('.wrap-form-field-password').find('input');
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
@endsection