@extends('layout')

@section('content')
    <section id="slideFaq">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Особистий кабінет</h2>
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <br>
Aenean commodo ligula eget dolor</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>ОСОБИСТИЙ КАБІНЕТ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="settings">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="excerpt-setting">
                        Якщо людина харчується поза домом, то її природнє бажання – насолодитися їжею, за яку вона заплатила.
                        Але дуже рідко ми можемо задовольнити свої смакові потреби. Адже далеко не у всіх закладах готують смачно.
                        Чому так? В Україні непристойно мало кваліфікованих кухарів.
                    </p>
                </div>
                @include('account.blocks.left-menu')
                <div class="col-lg-8">
                    <p>{{ session('message') }}</p>
                    <div class="list-instruction">
                        <ul>
                            @foreach($steps as $step)
                            <li>
                                <span class="main-span @if($loop->first || $loop->index < auth('account')->user()->step) active full-active @endif ">КРОК {{ $loop->iteration }}
                                    <span>{{ $step->name }}</span></span>
                                <p @if($loop->first || $loop->index < auth('account')->user()->step) style="display:block;" @endif>{!! $step->text !!}</p>
                                <a data-step="{{ $loop->index }}" class="checked-step @if($loop->index < auth('account')->user()->step) active @endif"
                                   href="javascript:void(0);"></a>
                            </li>
                            @endforeach
                        </ul>
                        <p>Або заповніть форму:</p>
                        <div class="wrap-wrap-mail">
                            <div class="mail-data">
                                У разі виникнення питань звертайтесь <a href="mailto:cultfood.ek@gmail.com">cultfood.ek@gmail.com</a>
                            </div>


                            <form action="{{ route('account.save-message') }}" method="post">
                                {{ csrf_field() }}
                                <textarea name="message" id="" cols="30" rows="10"></textarea>
                                <button type="submit" name="submit" value="">ВІДПРАВИТИ</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {

            var step = '{{ auth('account')->user()->step }}';
            $('.list-instruction ul li .main-span').click(function () {
                var el = $(this);

                var parent = el.closest('li');
                var index = parent.find('.checked-step').data('step');

                if(el.hasClass('full-active')) {
                    if(el.hasClass('active')) {
                        parent.find('p').slideUp(function () {
                            el.removeClass('active');
                        });
                    }
                    else {
                        el.addClass('active');
                        parent.find('p').slideDown();
                    }
                }



                if(step != index) return false;


                if(el.hasClass('active')) {
                    parent.find('p').slideUp(function () {
                        el.removeClass('active');
                    });
                }
                else {
                    el.addClass('active');
                    parent.find('p').slideDown();
                }
            });

            $('.list-instruction ul li a.checked-step').click(function () {
                var el = $(this);

                if(el.hasClass('active')) {
                    return false;
                }

                $.ajax({
                    type:'get',
                    url: '/account/save-step-account',
                    data: {step: el.data('step')},
                    success:function (result) {
                        step = +el.data('step') + 1;
                    }
                });

                el.addClass('active');



            });


        });
    </script>
@endsection
