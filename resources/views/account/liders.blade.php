@extends('layout')

@section('content')
    <section id="slideFaq">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Особистий кабінет</h2>
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <br>
Aenean commodo ligula eget dolor</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>ОСОБИСТИЙ КАБІНЕТ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="settings" class="padding-top-50">
        <div class="container">
            <div class="row">
                @include('account.blocks.left-menu')
                <div class="col-lg-8">

                    <div class="wrap-list-area list-area-style">
                        <ul>
                            <li style="display: block">ОБЕРІТЬ ОБЛАСТЬ</li>

                            @foreach($regions as $region)
                                <li><a href="" data-id="{{ $region->id }}">{{ $region->region }} область</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <section id="listLiders">
                        <ul>
                            {{--<li>--}}
                                {{--<img src="{{ asset('images/account-lider-image.jpg') }}" alt="">--}}
                                {{--<span class="name-lider">Marianne Loreno</span>--}}
                                {{--<span class="area-lider">Кропивницька область</span>--}}

                                {{--<ul>--}}
                                    {{--<li><span>Телефон:</span> <a href="">+38 246 582 24 59</a></li>--}}
                                    {{--<li><span>E-mail:</span> <a href="mailto:michaelandr@gmail.com">michaelandr@gmail.com</a></li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('javascript')
    <script>
        $(document).ready(function () {
            $('.col-lg-8 .wrap-list-area ul li a').click(function (e) {
                $('.wrap-list-area ul li a').removeClass('active');
                e.preventDefault();
                var el = $(this);
                el.addClass('active');
                var id = el.data('id');
                $('#listLiders ul').empty();
                $.ajax({
                    type: 'get',
                    url: '/show-liders',
                    data: {id:id},
                    success:function (result) {
                        $('#listLiders ul').append(result.html);
                    }
                });


                $.ajax({
                    type: 'get',
                    url: '/account/save-current-area',
                    data: {id:id}
                });

            });

            @if(auth('account')->user()->region_id)
                $('.wrap-list-area ul li a[data-id="{{ auth('account')->user()->region_id }}"]')[0].click();
            @endif
        });
    </script>
@endsection