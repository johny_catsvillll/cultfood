<div class="col-lg-4">
    <div class="wrap-list-area left-menu">
        <ul style="height:100%">
            <li>ОБЕРІТЬ ОБЛАСТЬ</li>
            <li><a @if(Request::path() == 'account/instruction') class="active" @endif href="{{ route('account.instruction') }}">ІНСТРУКЦІЯ</a></li>
            <li><a @if(Request::path() == 'account/liders') class="active" @endif href="{{ route('account.liders') }}">ЛІДЕР</a></li>
            <li><a @if(Request::path() == 'account') class="active" @endif href="{{ route('account.home') }}">ПЕРСОНАЛЬНІ ДАНІ</a></li>
            <li><a @if(Request::path() == 'account/chat') class="active" @endif href="{{ route('account.chat') }}">ПОВІДОМЛЕННЯ </a></li>
        </ul>
    </div>
</div>