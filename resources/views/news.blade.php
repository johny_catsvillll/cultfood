@extends('layout')

@section('content')
    <section id="slideFaq" class="news-page" @if($seo) style="background-image: url(/storage/{{ $seo->image }}) @endif">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $seo ? $seo->name : '' }}</h2>
                    <span>{{ $seo ? $seo->description : '' }}</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        @if(Request::path() == 'news')
                            <li><a>Новини проекту</a></li>
                        @else
                            <li><a>Блог</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="news" class="list-this-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-7">
                    <ul class="list-news">
                        @include('blocks.list-news')
                    </ul>
                    <input type="hidden" name="page" value="2">
                    @if($lastPage != 1)
                        <a href="" class="show-more">Показати ще</a>
                    @endif
                </div>
                @include('blocks.sidebar')
            </div>
        </div>
    </section>
@endsection

@section('javascript')

    <script>
        $(document).ready(function () {
            $('.show-more').click(function (e) {
                e.preventDefault();
                var page = $('input[name="page"]');

                var el = $(this);
                $.ajax({
                    type: 'get',
                    url: '/show-articles',
                    data: {page:page.val(), type: '{{ Request::path() }}'},
                    success:function (result) {
                        $('.list-news').append(result.html);

                        if(page.val() == result.lastPage || page.val() > result.lastPage) {
                            el.remove();
                        }

                        page.val(+page.val() + 1);
                    }
                });

            });
        });
    </script>

@endsection