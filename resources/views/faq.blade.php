@extends('layout')

@section('content')
    <section id="slideFaq" style="background-image: url(/storage/{{ $seo->image }})">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $seo->caption }}</h2>
                    <span>{{ $seo->description }}</span>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><a href="{{ url('/') }}">Головна</a></li>
                        <li><a>|</a></li>
                        <li><a>Faq</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="faq">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 offset-lg-1 col-md-4">
                    <img src="{{ asset('images/faq.png') }}" alt="">
                </div>
                <div class="col-lg-6 col-md-8">
                    <h3>Питання / Відповіді</h3>
                    <div class="list-questions">
                        <ul>
                            @foreach($questions as $question)
                                <li class="">
                                    <a href="">{{ $question->question }}</a>
                                    <div class="content-question">
                                        <div>{!! $question->answer !!}</div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('.list-questions ul li').on('click', function (e) {
                e.preventDefault();
                var el = $(this);

                if(el.hasClass('active')) {

                    el.find('.content-question').slideUp(function () {
                        el.removeClass('active');
                    });
                }
                else {
                    el.addClass('active');
                    el.find('.content-question').slideDown();
                }

            });
        });
    </script>
@endsection